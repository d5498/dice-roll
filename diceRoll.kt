
fun main() {
    val firstDice = Dice()
    // println(firstDice.sides)
    // firstDice.roll()
    val diceRoll = firstDice.roll()
    println("Your ${firstDice.sides} sided dice rolled at ${diceRoll}")
    
    
}
class Dice{
    var sides = 6
    
    fun roll(){
        val randomNumber = (1..6).random()
        // println("Random number : ${randomNumber}")
        return randomNumber
    }
}
